# Setup Grpc Dependency and Compile Proto

### Set GOPATH
```bash
export $GOPATH=""
```

### Download go grpc dependency
1. protoc-gen-go
2. proto
3. protoc-gen-grpc-gateway
4. protoc-gen-swagger
```bash
sh proto/get-deps.sh
```

### Download google proto library
1. http.proto
2. annotations.proto
3. any.proto
4. wrappers.proto
```bash
sh proto/get-googleapi.sh
```

### Modify REPONAME on proto/compile.sh file
```bash
{REPONAME}
```

### Create some proto file (example: health)
```bash
├── project
│   ├── proto
│   │   ├── v1
│   │   │   ├── health
│   │   │   │   ├── health.proto
│   ├── swagger
│   │   │   │   │ 
```

### Create go mod
```bash
go mod init
go mod tidy
```

### Compile proto file
```bash
sh proto/compile.sh
```
It will genereate .pb.go, .pb.gw.go and swagger docs.json file
```bash
├── project
│   ├── proto
│   │   ├── v1
│   │   │   ├── health
│   │   │   │   ├── health.proto
│   │   │   │   ├── health.pb.go
│   │   │   │   ├── health.pb.gw.go
│   │   │   │   
│   ├── swagger
│   │   ├── docs.json
│   │   │  
```