module gitlab.com/go-grpc-learning

go 1.17

require (
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	google.golang.org/genproto v0.0.0-20220624142145-8cd45d7dbd1f
	google.golang.org/grpc v1.47.0
	google.golang.org/protobuf v1.28.0
)

require (
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	golang.org/x/text v0.3.5 // indirect
)
